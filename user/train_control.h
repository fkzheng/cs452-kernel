#ifndef TRAIN_CONTROL_H
#define TRAIN_CONTROL_H

#include "pathfinding.h"
#include "physics/train_velocity.h"
#include "request.h"
#include "sensors.h"
#include "syscall.h"
#include "track_info/track.h"
#include "train_tests.h"
#include "util/util.h"

#define MAX_NUM_TRAINS 6

enum train_worker_request_type {
	TR,
	RV,
	RV_I,
};

struct Train_Worker_Request {
	enum train_worker_request_type request_type;
	char speed;
};

enum train_admin_request_type {
	TA_INIT,
	TA_CALIBRATION_BEGIN,			  // sent by terminal admin
	TA_CALIBRATION_END,				  // sent by calibration task to signal end of calibration
	TA_CALIBRATION_VELOCITY_HI,		  // calibrated velocity at high speed
	TA_CALIBRATION_VELOCITY_MED,	  // calibrated velocity at medium speed
	TA_CALIBRATION_VELOCITY_LO,		  // calibrated velocity at low speed
	TA_CALIBRATION_STOPPING_DISTANCE, // calibrated stopping distance
	TA_SPEED_UPDATE,
	TA_SENSOR_UPDATE,
	TA_PATH_UPDATE,
	TA_TRY_TO_START_PATH,
	TA_LOOP_STOP,
	TA_SET_TRAIN,
	TA_RNG,
	TA_LOCATE_TRAIN,
	TA_USER_TRAIN_COMMAND,
};

struct Train_Admin_Request {
	enum train_admin_request_type request_type;
	uint8_t train_num;
	uint32_t update;
	uint8_t src;
	uint8_t dest;
	uint16_t offset;
};

enum calibration_request_type {
	CR_START,
	CR_SENSOR,
};

struct Calibration_Request {
	enum calibration_request_type request_type;
	char value;
};

struct Switch_Request {
	uint8_t switch_nums[22];
	char directions[22];
	uint8_t sw_count;
};

void terminal_admin();
void train_admin();
void train_worker();
int turn_switches(char* switch_nums, char* directions, uint8_t sw_count);

void send_speed_command(char train_num, char speed);
void send_reverse_command(char train_num);
void reverse_instantly(char train_num);
void set_path2(uint8_t train, uint8_t dest, uint64_t offset);
void calibrate_train(char train);
void set_cur_train(char train);
void loop_stop(uint8_t dest);
int train_num_to_index(char train_num);
void train_calibration_task();
void toggle_random_paths();
void locate_train(int train_num, int sensor);
void control_user_train(char command);
#endif