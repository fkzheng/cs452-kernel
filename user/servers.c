#include "servers.h"

#define g_max_task_name_size 100
int g_name_server_tid;
void name_server() {
	// prints("Starting Name Server\r\n");
	int my_tid = MyTid();
	g_name_server_tid = my_tid;
	char map[g_max_ibuffer_elements + 1][100];
	int tid = 0;
	char buf[sizeof(struct Name_Server_Request)];
	// we're assuming here that we will only have task ids within 1-100 (inclusive)
	// returning 255 == -1 == not found. The first bit is never set if we find the task since it represents 128
	char replybuf[1];
	int found = 0;
	while (1) {
		Receive(&tid, buf, sizeof(struct Name_Server_Request));
		struct Name_Server_Request* request = (struct Name_Server_Request*)buf;
		switch ((enum name_server_request)request->request_type) {
		case NAME_SERVER_REGISTER:
			str_copy(request->name, map[request->tid]);
			replybuf[0] = 0;
			break;
		case NAME_SERVER_UNREGISTER:
			map[request->tid][0] = '\0';
			replybuf[0] = 0;
			break;
		case NAME_SERVER_WHOIS:
			found = 0;
			for (int i = 0; i < g_max_ibuffer_elements + 1; ++i) {
				if (str_equal(map[i], request->name)) {
					found = 1;
					replybuf[0] = i;
					// assuming no duplicates
					break;
				}
			}
			if (!found) {
				// Name not found
				replybuf[0] = -1;
			}
			break;
		}
		Reply(tid, replybuf, 1);
	}
}

uint64_t g_idle_server_tid;
void idle_server() {
	int my_tid = MyTid();
	g_idle_server_tid = my_tid;
	//	char* crocodile = "\033[?25l\033[40;1H\033[32m"
	//					  "Nigel is idling\r\n"
	//					  "                     _.---._     .---.\r\n"
	//					  "            __...---' .---. `---'-.   `.\r\n"
	//					  "  ~ -~ -.-''__.--' _.'( | )`.  `.  `._ :\r\n"
	//					  " -.~~ .'__-'_ .--'' ._`---'_.-.  `.   `-`.\r\n"
	//					  "  ~ ~_~-~-~_ ~ -._ -._``---. -.    `-._   `.\r\n"
	//					  "    ~- ~ ~ -_ -~ ~ -.._ _ _ _ ..-_ `.  `-._``--.._\r\n"
	//					  "     ~~-~ ~-_ _~ ~-~ ~ -~ _~~_-~ -._  `-.  -. `-._``--.._.--''. ~ -~_\r\n"
	//					  "         ~~ -~_-~ _~- _~~ _~-_~ ~-_~~ ~-.___    -._  `-.__   `. `. ~ -_~\r\n"
	//					  "             ~~ _~- ~~- -_~  ~- ~ - _~~- _~~ ~---...__ _    ._ .` `. ~-_~\r\n"
	//					  "                ~ ~- _~~- _-_~ ~-_ ~-~ ~_-~ _~- ~_~-_~  ~--.....--~ -~_ ~\r\n"
	//					  "                     ~ ~ - ~  ~ ~~ - ~~-  ~~- ~-  ~ -~ ~ ~ -~~-  ~- ~-~\r\n"
	//					  "\033[0m";

	for (;;) {
		Idle();
	}
	ASSERT(0);
	Exit();
}

void print_idle_time_server() {
	uint32_t idle_time_percentage_2_dec;
	prints("\0337\033[3;1HIdle Percentage: \0338");
	int clock_server_tid = get_tid_by_name(CLOCK_SERVER);
	char idle_time_string[] = "\0337\033[3;18HXX.XX%\0338";
	for (;;) {
		idle_time_percentage_2_dec = GetIdleTime();
		itos(idle_time_percentage_2_dec / 100, idle_time_string + 9, 0);
		if (idle_time_percentage_2_dec % 100 < 10) {
			idle_time_string[12] = '0';
			idle_time_string[13] = (idle_time_percentage_2_dec % 100) + '0';
		} else {
			itos(idle_time_percentage_2_dec % 100, idle_time_string + 12, 0);
		}
		prints(idle_time_string);
		Delay(clock_server_tid, 40);
	}
}
