#include "track_admin.h"

const char MAX_SWITCH_NUM = 18;
const char MIN_SWITCH_NUM = 1;
const char MIN_SWITCH_NUM_MIDDLE = 153;
const char MAX_SWITCH_NUM_MIDDLE = 156;
const char SWITCH_SOLENOID_OFF = 0x20;
const char SWITCH_STRAIGHT = 33;
const char SWITCH_CURVED = 34;
const int SOLENOID_OFF_DELAY = 10; // 200ms
const uint32_t PARKING_SPOT = UINT32_MAX;
const uint8_t SWITCH_SENSOR_LOOKAHEAD = 3;

static void send_switch_command(int tid, const char* cmd) {
	// we don't need the reply
	char reply[1];
	struct UART_Server_Request req = create_uart_request(UART_SEND_TRAIN, cmd);
	Send(tid, (char*)&req, sizeof(struct UART_Server_Request), reply, 1);
}

void print_switch_update(int switch_num, char switch_direction) {
	if (!((switch_num >= 0 && switch_num <= 18) || (switch_num >= 153 && switch_num <= 156))) {
		debug_printi("invalid switch_num: ", switch_num, "\r\n");
	}
	if (switch_direction != 'S' && switch_direction != 'C') {
		debug_printi("invalid switch direction, ascii: ", switch_direction, "\r\n");
	}
	uint8_t base_x = 8 + (((switch_num - 1) % 4) * 9);
	uint8_t base_y = (switch_num > 18) ? 16 : 6 + (((switch_num - 1) / 4) * 2);

	char update[] = "\0337\033[31m\033[41;67H_\033[0m\0338";
	update[15] = switch_direction;
	update[9] = (base_y / 10) + '0';
	update[10] = (base_y % 10) + '0';
	update[12] = (base_x / 10) + '0';
	update[13] = (base_x % 10) + '0';

	prints(update);
}

void _print_switch_table(int clock_server_tid) {
	char ui_config[] = "\0337\033[35m";
	char ui_reset[] = "\033[0m\0338";
	char switch_ui1[] = "\033[05;1H╔════════╦════════╦════════╦════════╗"
						"\033[06;1H║ 01:  ? ║ 02:  ? ║ 03:  ? ║ 04:  ? ║"
						"\033[07;1H╠════════╬════════╬════════╬════════╣"
						"\033[08;1H║ 05:  ? ║ 06:  ? ║ 07:  ? ║ 08:  ? ║"
						"\033[09;1H╠════════╬════════╬════════╬════════╣"
						"\033[10;1H║ 09:  ? ║ 10:  ? ║ 11:  ? ║ 12:  ? ║"
						"\033[11;1H╠════════╬════════╬════════╬════════╣";
	char switch_ui2[] = "\033[12;1H║ 13:  ? ║ 14:  ? ║ 15:  ? ║ 16:  ? ║"
						"\033[13;1H╠════════╬════════╬════════╬════════╣"
						"\033[14;1H║ 17:  ? ║ 18:  ? ║        ║        ║"
						"\033[15;1H╠════════╬════════╬════════╬════════╣"
						"\033[16;1H║ 153: ? ║ 154: ? ║ 155: ? ║ 156: ? ║"
						"\033[17;1H╚════════╩════════╩════════╩════════╝";
	char output1[sizeof(ui_config) + sizeof(ui_reset) + sizeof(switch_ui1) + 1] = { 0 };
	char output2[sizeof(ui_config) + sizeof(ui_reset) + sizeof(switch_ui2) + 1] = { 0 };
	str_copy(ui_config, output1);
	str_cat(output1, switch_ui1);
	str_cat(output1, ui_reset);

	str_copy(ui_config, output2);
	str_cat(output2, switch_ui2);
	str_cat(output2, ui_reset);
	// make this puts
	prints(output1);
	Delay(clock_server_tid, 10);
	prints(output2);
}

void _print_track(int clock_server_tid) {
	char track1[] = "\0337"
					"\033[27;1H▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁o▁"
					"\033[28;1H                       ╱                 ╲                  ╲                       "
					"\033[29;1H     ▁▁▁▁▁▁▁▁o▁▁▁▁▁▁▁▁╱▁▁▁o▁▁▁▁▁▁▁▁▁▁o▁▁▁▁╲▁▁▁▁▁▁▁▁o▁▁▁▁▁    ╲▁▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁▁▁o▁"
					"\0338";
	char track2[] = "\0337"
					"\033[30;1H    ╱                                                    ╲    ╲                     "
					"\033[31;1H   ╱ ▁▁▁▁▁▁▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁o▁▁▁▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁o▁▁▁▁ ╲    ╲▁▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁o▁"
					"\033[32;1H  o ╱                     ╲           ╱                  ╲ ╲    ╲                   "
					"\0338";
	char track3[] = "\0337"
					"\033[33;1H ╱ o                       ╲         ╱                    ╲ ╲    ╲                  "
					"\033[34;1H⧫ ╱                         o       o                      ╲ ╲    ╲                 "
					"\033[35;1H│╱                           ╲  │  ╱                        ╲ ╲    ⧫                "
					"\0338";
	char track4[] = "\0337"
					"\033[36;1H│                             o │ o                          ╲ ⧫   │                "
					"\033[37;1H│                              ╲│╱                            ╲│   o                "
					"\033[38;1H│                               ▼                              o   │                "
					"\0338";
	char track5[] = "\0337"
					"\033[39;1H│                               │                              │   │                "
					"\033[40;1H│                               ▲                              │   │                "
					"\033[41;1H│                              ╱│╲                             o   │                "
					"\0338";
	char track6[] = "\0337"
					"\033[42;1H│                             ╱ │ ╲                           ╱│   o                "
					"\033[43;1H│                            o  │  o                         ╱ ⧫   │                "
					"\033[44;1H│╲                          ╱       ╲                       ╱ ╱    ⧫                "
					"\0338";
	char track7[] = "\0337"
					"\033[45;1H⧫ ╲                        o         o                     ╱ ╱    ╱                 "
					"\033[46;1H ╲ o                      ╱           ╲                   ╱ ╱    ╱                  "
					"\033[47;1H  o ╲▁▁▁▁▁▁▁▁▁▁o▁▁▁▁▁▁▁▁▁╱▁▁o▁▁▁▁▁▁▁o▁▁╲▁▁▁▁▁▁▁▁o▁▁▁▁▁▁▁▁╱ ╱    ╱▁▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁▁"
					"\0338";
	char track8[] = "\0337"
					"\033[48;1H   ╲                                                      ╱    ╱                    "
					"\033[49;1H    ╲▁▁▁▁▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁o▁▁▁▁▁▁╱▁▁▁▁╱▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁"
					"\0338";
	prints(track1);
	Delay(clock_server_tid, 10);
	prints(track2);
	Delay(clock_server_tid, 10);
	prints(track3);
	Delay(clock_server_tid, 10);
	prints(track4);
	Delay(clock_server_tid, 10);
	prints(track5);
	Delay(clock_server_tid, 10);
	prints(track6);
	Delay(clock_server_tid, 10);
	prints(track7);
	Delay(clock_server_tid, 10);
	prints(track8);
}

void _print_track_a(int clock_server_tid) {
	char track1[] = "\0337"
					"\033[27;1H▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁o▁"
					"\033[28;1H                       ╱                 ╲                  ╲                       "
					"\033[29;1H     ▁▁▁▁▁▁▁▁o▁▁▁▁▁▁▁▁╱▁▁▁o▁▁▁▁▁▁▁▁▁▁o▁▁▁▁╲▁▁▁▁▁▁▁▁o▁▁▁▁▁    ╲▁▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁▁▁o▁"
					"\0338";
	char track2[] = "\0337"
					"\033[30;1H    ╱                                                    ╲    ╲                     "
					"\033[31;1H   ╱ ▁▁▁▁▁▁▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁o▁▁▁▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁o▁▁▁▁ ╲    ╲▁▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁o▁"
					"\033[32;1H  o ╱                     ╲           ╱                  ╲ ╲    ╲                   "
					"\0338";
	char track3[] = "\0337"
					"\033[33;1H ╱ o                       ╲         ╱                    ╲ ╲    ╲▁▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁"
					"\033[34;1H⧫ ╱                         o       o                      ╲ ╲                      "
					"\033[35;1H│╱                           ╲  │  ╱                        ╲ ╲                     "
					"\0338";
	char track4[] = "\0337"
					"\033[36;1H│                             o │ o                          ╲ ⧫                    "
					"\033[37;1H│                              ╲│╱                            ╲│                    "
					"\033[38;1H│                               ▼                              o                    "
					"\0338";
	char track5[] = "\0337"
					"\033[39;1H│                               │                              │                    "
					"\033[40;1H│                               ▲                              │                    "
					"\033[41;1H│                              ╱│╲                             o                    "
					"\0338";
	char track6[] = "\0337"
					"\033[42;1H│                             ╱ │ ╲                           ╱│                    "
					"\033[43;1H│                            o  │  o                         ╱ ⧫                    "
					"\033[44;1H│╲                          ╱       ╲                       ╱ ╱                     "
					"\0338";
	char track7[] = "\0337"
					"\033[45;1H⧫ ╲                        o         o                     ╱ ╱    ▁▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁"
					"\033[46;1H ╲ o                      ╱           ╲                   ╱ ╱    ╱                  "
					"\033[47;1H  o ╲▁▁▁▁▁▁▁▁▁▁o▁▁▁▁▁▁▁▁▁╱▁▁o▁▁▁▁▁▁▁o▁▁╲▁▁▁▁▁▁▁▁o▁▁▁▁▁▁▁▁╱ ╱    ╱▁▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁▁"
					"\0338";
	char track8[] = "\0337"
					"\033[48;1H   ╲                                                      ╱    ╱                    "
					"\033[49;1H    ╲▁▁▁▁▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁o▁▁▁▁▁▁╱▁▁▁▁╱▁▁▁▁o▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁"
					"\0338";
	prints(track1);
	Delay(clock_server_tid, 10);
	prints(track2);
	Delay(clock_server_tid, 10);
	prints(track3);
	Delay(clock_server_tid, 10);
	prints(track4);
	Delay(clock_server_tid, 10);
	prints(track5);
	Delay(clock_server_tid, 10);
	prints(track6);
	Delay(clock_server_tid, 10);
	prints(track7);
	Delay(clock_server_tid, 10);
	prints(track8);
}

void _print_node_reservation(int node, char reserved) {
	ASSERT(node <= TRACK_NODE_COUNT);
	char save_cursor[] = "\0337";
	char ui_reset[] = "\033[0m\0338";
	char ui_colour[] = "\033[32m";
	if (reserved) {
		ui_colour[3] = '1'; // red for reserved
	}

	int row = (node / 16) + 18;
	int col = ((node % 16) * 6) + 1;
	char move_cursor[] = "\033[00;000H";
	move_cursor[2] = (row / 10) + '0';
	move_cursor[3] = (row % 10) + '0';
	move_cursor[5] = (col / 100) + '0';
	move_cursor[6] = ((col / 10) % 10) + '0';
	move_cursor[7] = (col % 10) + '0';

	char final_msg[charsize(save_cursor) + charsize(ui_colour) + charsize(move_cursor) + charsize(ui_reset) + 6];
	str_copy(save_cursor, final_msg);
	str_cat(final_msg, ui_colour);
	str_cat(final_msg, move_cursor);
	str_cat(final_msg, track_nodes[node].name);
	str_cat(final_msg, ui_reset);
	prints(final_msg);

	// print on track by sending to the track ui server
	if (0 <= node && node < 80) {
		reserved ? update_track_ui(TRACK_UI_SENSOR_RESERVE, node, 0)
				 : update_track_ui(TRACK_UI_SENSOR_UNRESERVE, node, 0);
	}
}

void _print_reservation_table(uint32_t blocked[144][6][2], uint32_t curtime, char cur_res_screen_state[144]) {
	char overlapping;
	for (int i = 0; i < 144; i++) {
		overlapping = 0;
		for (int j = 0; j < 6; j++) {
			if (blocked[i][j][0] <= curtime && curtime <= blocked[i][j][1]) {
				overlapping = 1;
			}
		}

		if (overlapping && cur_res_screen_state[i] == 0) {
			_print_node_reservation(i, 1);
			cur_res_screen_state[i] = 1;
		}
		if (!overlapping && cur_res_screen_state[i] == 1) {
			_print_node_reservation(i, 0);
			cur_res_screen_state[i] = 0;
		}
	}
}

int can_reserve_node(uint32_t track_node_reservations[144][6][2],
					 uint8_t node,
					 uint32_t start_time,
					 uint32_t end_time,
					 uint32_t curtime) {
	ASSERT(node <= 143);
	int has_free_slot = 0;
	for (int j = 0; j < 6; j++) {
		if (track_node_reservations[node][j][0] != 0 && track_node_reservations[node][j][0] <= end_time
			&& start_time <= track_node_reservations[node][j][1]) {
			return 0; // failed, overlapping reservation, break short
		}
		if (track_node_reservations[node][j][1] == 0 || track_node_reservations[node][j][1] < curtime) {
			has_free_slot = 1;
		}
	}
	if (!has_free_slot) {
		return 0; // we wouldn't have anywhere to store the reservation, return false
	}
	return 1;
}

int any_conflicting_reservations(uint32_t track_node_reservations[144][6][2],
								 uint8_t* track_nodes_encoded,
								 uint8_t node_count,
								 uint32_t start_time,
								 uint32_t end_time,
								 uint32_t curtime) {
	uint8_t node = 0;
	uint8_t rev = 0;
	for (uint8_t i = 0; i < node_count; i++) {
		node = track_nodes_encoded[i];
		ASSERT(node <= 143);
		rev = track_nodes[node].edge[DIR_STRAIGHT].dest->reverse->idx;
		if (!can_reserve_node(track_node_reservations, node, start_time, end_time, curtime)) {
			return 1;
		}
		if (!can_reserve_node(track_node_reservations, rev, start_time, end_time, curtime)) {
			return 1;
		}
		if (track_nodes[node].type == NODE_BRANCH) {
			rev = track_nodes[node].edge[DIR_CURVED].dest->reverse->idx;
			if (!can_reserve_node(track_node_reservations, rev, start_time, end_time, curtime)) {
				return 1;
			}
		}
	}
	return 0;
}

void add_node_reservation(uint32_t track_node_reservations[144][6][2],
						  uint8_t node,
						  uint32_t start_time,
						  uint32_t end_time,
						  uint32_t curtime,
						  char parking) {
	char updated;
	if (parking) {
		track_node_reservations[node][0][0] = 0;
		track_node_reservations[node][0][1] = PARKING_SPOT;
	} else {
		updated = 0;
		for (int j = 0; j < 6; j++) {
			if (track_node_reservations[node][j][1] == 0 || track_node_reservations[node][j][1] < curtime) {
				updated = 1;
				track_node_reservations[node][j][0] = start_time;
				track_node_reservations[node][j][1] = end_time;
				break;
			}
		}
		if (!updated) {
			debug_printi("could not reserve node ", node, " this could be because of parking!\r\n");
			ASSERT(0);
		}
	}
}

// adds the reservations to the given list, assume the above function is already called and returned 0
void update_reservations(uint32_t track_node_reservations[144][6][2],
						 uint8_t* track_nodes_encoded,
						 uint8_t node_count,
						 uint32_t start_time,
						 uint32_t end_time,
						 uint32_t curtime) {
	char debug_msg_prefix[] = "Track Admin reserved nodes: ";
	char debug_msg_suffix[] = "\r\n";
	char final_msg[charsize(debug_msg_prefix) + (node_count * 5) + charsize(debug_msg_suffix)];
	str_copy(debug_msg_prefix, final_msg);
	uint8_t node = 0;
	uint8_t rev = 0;
	for (uint8_t i = 0; i < node_count; i++) {
		node = track_nodes_encoded[i];
		rev = track_nodes[node].edge[DIR_AHEAD].dest->reverse->idx;
		str_cat(final_msg, track_nodes[node].name);
		str_cat(final_msg, " ");
		add_node_reservation(track_node_reservations, node, start_time, end_time, curtime, 0);
		add_node_reservation(track_node_reservations, rev, start_time, end_time, curtime, 0);
		if (track_nodes[node].type == NODE_BRANCH) {
			rev = track_nodes[node].edge[DIR_CURVED].dest->reverse->idx;
			add_node_reservation(track_node_reservations, rev, start_time, end_time, curtime, 0);
		}
	}
	str_cat(final_msg, debug_msg_suffix);
	// debug_prints(final_msg);
	// debug_printi("start time: ", start_time, "\r\n");
	// debug_printi("end time: ", end_time, "\r\n");
}

uint8_t reverse_node(uint8_t node) {
	ASSERT(node <= 144);
	ASSERT(track_nodes[node].reverse);
	return track_nodes[node].reverse->idx;
}

uint8_t reverse_node_for_same_segment(uint8_t node, uint8_t direction) {
	if (track_nodes[node].type != NODE_EXIT) {
		return track_nodes[node].edge[direction].dest->reverse->idx;
	}
	return 0xFF;
}

uint8_t dedup_array(uint8_t* buf, uint8_t count) {
	uint8_t new_arr[40];
	uint8_t new_count = 0;
	uint8_t existing = 0;
	for (int i = 0; i < count; i++) {
		existing = 0;
		for (int j = 0; j < new_count; j++) {
			if (new_arr[j] == buf[i]) {
				existing = 1;
			}
		}
		if (!existing) {
			new_arr[new_count++] = buf[i];
		}
	}
	for (int i = 0; i < new_count; i++) {
		buf[i] = new_arr[i];
	}
	return new_count;
}

// buf should be at least 40 nodes
uint8_t get_nodes_for_parking(uint8_t node, uint8_t* buf) {
	int train_len = 210 + 50; // add 5cm buffer on each side
	int queue[40];
	int dist_queue[40];

	int front = 0, rear = 0;
	uint8_t count = 0;

	queue[rear] = node;
	dist_queue[rear++] = 0;
	queue[rear] = reverse_node(node);
	dist_queue[rear++] = 0;

	struct track_node trk_node;
	int distance = 0;
	uint8_t rev_node;
	while (front != rear && front < 40) {
		node = queue[front];
		distance = dist_queue[front++];
		trk_node = track_nodes[node];
		if (distance > train_len) {
			continue;
		}
		buf[count++] = node;
		rev_node = reverse_node_for_same_segment(node, DIR_STRAIGHT);

		if (rev_node != 0xFF) {
			buf[count++] = rev_node;
		}

		if (trk_node.type == NODE_BRANCH) {
			buf[count++] = reverse_node_for_same_segment(node, DIR_CURVED);
		}

		if (trk_node.type == NODE_EXIT) {
			continue;
		}

		queue[rear] = trk_node.edge[DIR_AHEAD].dest->idx;
		dist_queue[rear++] = trk_node.edge[DIR_AHEAD].dist + distance;
		if (trk_node.type == NODE_BRANCH) {
			queue[rear] = trk_node.edge[DIR_CURVED].dest->idx;
			dist_queue[rear++] = trk_node.edge[DIR_CURVED].dist + distance;
		} else if (trk_node.type == NODE_MERGE) {
			queue[rear] = reverse_node(node);
			dist_queue[rear++] = distance;
		}
	}
	ASSERT(count < 40);
	return dedup_array(buf, count);
}

void remove_node_parking_reservation(uint32_t track_node_reservations[144][6][2], uint8_t node) {
	// clears all resrevations from a node
	ASSERT(node <= 144);
	for (int i = 0; i < 6; i++) {
		if (track_node_reservations[node][i][1] == PARKING_SPOT) {
			track_node_reservations[node][i][0] = 0;
			track_node_reservations[node][i][1] = 0;
		}
	}
}

void _park_train(uint32_t track_node_reservations[144][6][2], uint8_t parking_spot) {
	uint8_t nearby_nodes[40];
	uint8_t node_count = get_nodes_for_parking(parking_spot, nearby_nodes);
	char debug_msg_prefix[] = "Reserved nodes for parking: ";
	char debug_msg_suffix[] = "\r\n";
	char final_msg[charsize(debug_msg_prefix) + (node_count * 2 * 5) + charsize(debug_msg_suffix)];
	str_copy(debug_msg_prefix, final_msg);
	uint8_t node = 0;
	for (uint8_t i = 0; i < node_count; i++) {
		node = nearby_nodes[i];
		add_node_reservation(track_node_reservations, node, 0, 0, 0, 1);
		str_cat(final_msg, track_nodes[node].name);
		str_cat(final_msg, " ");
	}
	str_cat(final_msg, debug_msg_suffix);
	// debug_prints(final_msg);
}

void _unpark_train(uint32_t track_node_reservations[144][6][2], uint8_t parking_spot) {
	uint8_t nearby_nodes[40];
	uint8_t node_count = get_nodes_for_parking(parking_spot, nearby_nodes);
	char debug_msg_prefix[] = "Unreserved parking nodes: ";
	char debug_msg_suffix[] = "\r\n";
	char final_msg[charsize(debug_msg_prefix) + (node_count * 2 * 5) + charsize(debug_msg_suffix)];
	str_copy(debug_msg_prefix, final_msg);
	uint8_t node = 0;
	for (uint8_t i = 0; i < node_count; i++) {
		node = nearby_nodes[i];
		remove_node_parking_reservation(track_node_reservations, node);
		str_cat(final_msg, track_nodes[node].name);
		str_cat(final_msg, " ");
	}
	str_cat(final_msg, debug_msg_suffix);
	// debug_prints(final_msg);
}

void print_res_task() {
	int clock_server_tid = get_tid_by_name(CLOCK_SERVER);
	int track_admin_tid = get_tid_by_name(TRACK_ADMIN);
	struct Track_Request req = { .request_type = TRACK_PRINT_RES_MAYBE };
	char reply[] = "0";
	while (1) {
		Delay(clock_server_tid, 50);
		Send(track_admin_tid, (char*)&req, sizeof(struct Track_Request), reply, 1);
	}
}

void _print_reservation_table_start() {
	// int clock_server_tid = get_tid_by_name(CLOCK_SERVER);
	char ui_config[] = "\0337\033[32m";
	char ui_reset[] = "\033[0m\0338";
	for (int i = 0; i < 9; i++) {
		// Delay(clock_server_tid, 100);
		char move_cursor[] = "\033[00;01H";
		move_cursor[2] = ((i + 18) / 10) + '0';
		move_cursor[3] = ((i + 18) % 10) + '0';
		char final_msg[charsize(move_cursor) + charsize(ui_config) + charsize(ui_reset) + (6 * 16)];
		str_copy(ui_config, final_msg);
		str_cat(final_msg, move_cursor);
		for (int j = 0; j < 16 && (i * 16) + j < TRACK_NODE_COUNT; j++) {
			str_cat(final_msg, track_nodes[(i * 16) + j].name);
			for (int k = 0; k < (6 - charsize(track_nodes[(i * 16) + j].name)) + 1; k++) {
				str_cat(final_msg, " ");
			}
		}
		str_cat(final_msg, ui_reset);
		prints(final_msg);
	}
}

// TODO: have this server communicate current switch positions
void track_admin() {
	RegisterAs(TRACK_ADMIN);
	// 0: C 1: S
	int sender;
	char msg[sizeof(struct Track_Request)];
	char reply[1] = { 0 };
	char switch_states[157] = { 0 };
	uint32_t track_node_reservations[144][6][2] = { 0 };
	char cur_res_screen_state[144] = { 0 };
	int clock_server_tid = get_tid_by_name(CLOCK_SERVER);
	uint32_t curtime;

	Delay(clock_server_tid, 250);

	_print_switch_table(clock_server_tid);
	_print_reservation_table_start();

	Create(3, print_res_task);

	for (;;) {
		prints("\0337\033[1;45H\033[32mTA\033[0m\0338");
		Receive(&sender, msg, sizeof(struct Track_Request));
		prints("\0337\033[1;45H\033[31mTA\033[0m\0338");
		curtime = Time(clock_server_tid);
		struct Track_Request* req = (struct Track_Request*)msg;

		switch (req->request_type) {
		case TRACK_UPDATE_SWITCH:
			Reply(sender, reply, 1);
			for (int i = 0; i < req->sw_count; i++) {
				switch_states[(uint8_t)req->switch_nums[i]] = req->directions[i];
				print_switch_update(req->switch_nums[i], req->directions[i]);
			}
			break;
		case TRACK_PRINT_RES_MAYBE:
			Reply(sender, reply, 1);
			_print_reservation_table(track_node_reservations, curtime, cur_res_screen_state);
			break;
		case TRACK_GET_SWITCH:
			reply[0] = switch_states[(int)req->switch_num];
			Reply(sender, reply, 1);

			break;
		case TRACK_RESERVE_SEGMENTS:
			reply[0] = 1;
			if (any_conflicting_reservations(track_node_reservations,
											 req->track_nodes_encoded,
											 req->node_count,
											 req->reservation_time[0],
											 req->reservation_time[1],
											 curtime)) {
				debug_printi("Track server could not reserve node: ", req->track_nodes_encoded[0], "\r\n");
				debug_printi("num nodes requested: ", req->node_count, "\r\n");
				ASSERT(0);
				reply[0] = 0;
			} else {
				update_reservations(track_node_reservations,
									req->track_nodes_encoded,
									req->node_count,
									req->reservation_time[0],
									req->reservation_time[1],
									curtime);
			}
			Reply(sender, reply, 1);
			break;
		case TRACK_UNPARK:
			ASSERT(req->node_count == 1);
			reply[0] = 1;
			Reply(sender, reply, 1);
			_unpark_train(track_node_reservations, req->track_nodes_encoded[0]);

			break;
		case TRACK_PARK:
			ASSERT(req->node_count == 1);
			reply[0] = 1;
			_park_train(track_node_reservations, req->track_nodes_encoded[0]);
			Reply(sender, reply, 1);
			break;
		case TRACK_GET_RESERVATIONS:
			Reply(sender, track_node_reservations, sizeof(uint32_t) * 144 * 6 * 2);
			break;
		default:
			ASSERT(0);
			break;
		}
	}

	Exit();
}

int _is_in_list(int list[80], int size, int value) {
	for (int i = 0; i < size; i++) {
		if (list[i] == value) {
			return 1;
		}
	}
	return 0;
}

void _print_train_on_track(uint8_t train_num, int sensor_id) {
	char save_cursor[] = "\0337";
	char ui_reset[] = "\033[0m\0338";
	char track_msg[50];
	int draw_vertical[] = { 2,	3,	18, 19, 28, 29, 30, 31, 32, 33, 48, 49, 52, 53,
							54, 55, 56, 57, 62, 63, 64, 65, 66, 67, 72, 73, 78, 79 };
	int draw_horisontal[]
		= { 0,	1,	4,	5,	6,	7,	8,	9,	10, 11, 12, 13, 14, 15, 16, 17, 20, 21, 22, 23, 24, 25, 26, 27, 34, 35,
			36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 50, 51, 58, 59, 60, 61, 68, 69, 70, 71, 74, 75, 76, 77 };

	str_copy(save_cursor, track_msg);
	switch (train_num) {
	case 24:
		str_cat(track_msg, "\033[35m");
		break;
	case 58:
		str_cat(track_msg, "\033[34m");
		break;
	case 78:
		str_cat(track_msg, "\033[36m");
		break;
	default:
		debug_prints("ERROR invalid train to print\r\n");
		break;
	}

	str_cat(track_msg, track_nodes[sensor_id].image);
	if (_is_in_list(draw_vertical, 28, sensor_id)) {
		str_cat(track_msg, "█");
	} else if (_is_in_list(draw_horisontal, 52, sensor_id)) {
		str_cat(track_msg, "▅▅");
	} else {
		ASSERT(0);
	}

	str_cat(track_msg, ui_reset);
	prints(track_msg);
}

void _reset_sensor_on_track(int sensor_id, char sensor_state) {
	char save_cursor[] = "\0337";
	char ui_reset[] = "\033[0m\0338";
	char track_msg[50];
	int draw_vertical[] = { 2,	3,	18, 19, 28, 29, 30, 31, 32, 33, 48, 49, 52, 53,
							54, 55, 56, 57, 62, 63, 64, 65, 66, 67, 72, 73, 78, 79 };
	int draw_horisontal[]
		= { 0,	1,	4,	5,	6,	7,	8,	9,	10, 11, 12, 13, 14, 15, 16, 17, 20, 21, 22, 23, 24, 25, 26, 27, 34, 35,
			36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 50, 51, 58, 59, 60, 61, 68, 69, 70, 71, 74, 75, 76, 77 };

	str_copy(save_cursor, track_msg);
	switch (sensor_state) {
	case 0:
		str_cat(track_msg, "\033[37m");
		break;
	case 1:
		str_cat(track_msg, "\033[31m");
		break;
	}
	str_cat(track_msg, track_nodes[sensor_id].image);
	if (_is_in_list(draw_vertical, 28, sensor_id)) {
		str_cat(track_msg, "o");
	} else if (_is_in_list(draw_horisontal, 52, sensor_id)) {
		str_cat(track_msg, "o\033[0m▁");
	} else {
		ASSERT(0);
	}

	str_cat(track_msg, ui_reset);
	prints(track_msg);
}

void track_ui_admin() {
	RegisterAs(TRACK_UI_ADMIN);
	int clock_server_tid = get_tid_by_name(CLOCK_SERVER);
	int sender;
	char msg[sizeof(struct Track_UI_Request)];
	struct Track_UI_Request* req = (struct Track_UI_Request*)msg;
	char reply[1] = { 0 };
	// 0: free
	// 1: reserved
	char sensor_state[80] = { 0 };
	int train_loc[6] = { 0 };

	// print the track on boot
	if (which_track == 'b') {
		_print_track(clock_server_tid);
	} else {
		_print_track_a(clock_server_tid);
	}

	for (;;) {
		Receive(&sender, msg, sizeof(struct Track_UI_Request));
		Reply(sender, reply, 1);

		switch (req->request_type) {
		case TRACK_UI_SENSOR_RESERVE:
			sensor_state[req->sensor_num] = 1;
			_reset_sensor_on_track(req->sensor_num, sensor_state[req->sensor_num]);
			break;
		case TRACK_UI_SENSOR_UNRESERVE:
			sensor_state[req->sensor_num] = 0;
			_reset_sensor_on_track(req->sensor_num, sensor_state[req->sensor_num]);
			break;
		case TRACK_UI_SENSOR_TRAIN:
			// reset the existing location, print on new location
			if (req->train_num == 24) {
				_reset_sensor_on_track(train_loc[2], sensor_state[train_loc[2]]);
				train_loc[2] = req->sensor_num;
			} else if (req->train_num == 58) {
				_reset_sensor_on_track(train_loc[3], sensor_state[train_loc[3]]);
				train_loc[3] = req->sensor_num;
			} else if (req->train_num == 78) {
				_reset_sensor_on_track(train_loc[5], sensor_state[train_loc[5]]);
				train_loc[5] = req->sensor_num;
			} else {
				debug_prints("INVALID TRAIN PASSED TO UI\r\n");
			}
			break;
		default:
			break;
		}

		_print_train_on_track(24, train_loc[2]);
		_print_train_on_track(58, train_loc[3]);
		_print_train_on_track(78, train_loc[5]);
	}
}

void update_track_ui(enum track_ui_request type, int sensor_num, int train_num) {
	int tid = get_tid_by_name(TRACK_UI_ADMIN);
	char replybuf[1];
	struct Track_UI_Request req = { .request_type = type, .sensor_num = sensor_num, .train_num = train_num };
	Send(tid, (char*)&req, sizeof(struct Track_UI_Request), replybuf, 1);
}

void flip_next_switches(struct Ring_Buffer* path) {
	char switches[22];
	char directions[22];
	uint8_t sw_count = 0;
	uint8_t sensor_counter = 0;

	for (int i = 0; i < path->size - 1; i++) {
		uint8_t node_idx = path->cbuffer[(path->l + i) % g_max_cbuffer_elements];
		struct track_node node = track_nodes[node_idx];

		if (sensor_counter == SWITCH_SENSOR_LOOKAHEAD) {
			break;
		} else if (node.type == NODE_SENSOR) {
			sensor_counter++;
		} else if (node.type == NODE_BRANCH) {
			switches[sw_count] = node.num;
			if (node.edge[DIR_STRAIGHT].dest->idx == path->cbuffer[(path->l + i + 1) % g_max_cbuffer_elements]) {
				directions[sw_count] = 'S';
			} else if (node.edge[DIR_CURVED].dest->idx == path->cbuffer[(path->l + i + 1) % g_max_cbuffer_elements]) {
				directions[sw_count] = 'C';
			} else {
				ASSERT(0);
			}
			sw_count++;
		}
	}
	sw_count = turn_switches(switches, directions, sw_count);
	// printi("Flipping ", sw_count, " switches\r\n");
	// Delay(get_tid_by_name(CLOCK_SERVER), 100);
}

char get_switch_state(uint8_t switch_num) {
	int track_admin_tid = get_tid_by_name(TRACK_ADMIN);

	// prints("DEBUG\r\n");
	debug_printi("switch_num: ", switch_num, "\r\n");
	struct Track_Request track_req = { .request_type = TRACK_GET_SWITCH, .switch_num = switch_num };
	char reply[1];
	Send(track_admin_tid, (char*)&track_req, sizeof(struct Track_Request), reply, 1);
	ASSERT((reply[0] == 'S') || (reply[0] == 'C') || (reply[0] == 0));
	return reply[0];
}

void switch_worker() {
	char msg[sizeof(struct Switch_Request)];
	int sender_tid = 0;
	Receive(&sender_tid, msg, sizeof(struct Switch_Request));
	Reply(sender_tid, msg, 1);

	struct Switch_Request* req = (struct Switch_Request*)msg;
	struct Track_Request track_req = { .request_type = TRACK_UPDATE_SWITCH, .sw_count = 0 };
	int uart_train_tx_tid = get_tid_by_name(UART_SERVER_TRAIN_SEND);
	char cmd[] = "  ";
	for (int i = 0; i < req->sw_count; i++) {
		// check if we need to flip it
		if (req->directions[i] != get_switch_state(req->switch_nums[i])) {
			cmd[0] = req->directions[i] == 'S' ? SWITCH_STRAIGHT : SWITCH_CURVED;
			cmd[1] = req->switch_nums[i];

			// send switch command
			if (req->switch_nums[i] == 153 || req->switch_nums[i] == 154) {
				send_switch_command(uart_train_tx_tid, cmd);
				track_req.switch_nums[track_req.sw_count] = req->switch_nums[i];
				track_req.directions[track_req.sw_count] = req->directions[i];
				track_req.sw_count++;
				// flip other switch
				cmd[1] = req->switch_nums[i] == 153 ? 154 : 153;
				cmd[0] = req->directions[i] == 'S' ? SWITCH_CURVED : SWITCH_STRAIGHT;
				send_switch_command(uart_train_tx_tid, cmd);
				track_req.switch_nums[track_req.sw_count] = cmd[1];
				track_req.directions[track_req.sw_count] = req->directions[i] == 'S' ? 'C' : 'S';
				track_req.sw_count++;
			} else if (req->switch_nums[i] == 155 || req->switch_nums[i] == 156) {
				send_switch_command(uart_train_tx_tid, cmd);
				track_req.switch_nums[track_req.sw_count] = req->switch_nums[i];
				track_req.directions[track_req.sw_count] = req->directions[i];
				track_req.sw_count++;
				// flip other switch
				cmd[1] = req->switch_nums[i] == 155 ? 156 : 155;
				cmd[0] = req->directions[i] == 'S' ? SWITCH_CURVED : SWITCH_STRAIGHT;
				send_switch_command(uart_train_tx_tid, cmd);
				track_req.switch_nums[track_req.sw_count] = cmd[1];
				track_req.directions[track_req.sw_count] = req->directions[i] == 'S' ? 'C' : 'S';
				track_req.sw_count++;
			} else {
				send_switch_command(uart_train_tx_tid, cmd);
				track_req.switch_nums[track_req.sw_count] = req->switch_nums[i];
				track_req.directions[track_req.sw_count] = req->directions[i];
				track_req.sw_count++;
			}
		}
	}

	// send solenoid off
	cmd[0] = SWITCH_SOLENOID_OFF;
	cmd[1] = '\0';
	if (track_req.sw_count) {
		Delay(get_tid_by_name(CLOCK_SERVER), SOLENOID_OFF_DELAY);
		send_switch_command(uart_train_tx_tid, cmd);
		char reply[1];
		Send(get_tid_by_name(TRACK_ADMIN), (char*)&track_req, sizeof(struct Track_Request), reply, 1);
	}
	debug_prints("Switch worker exiting\r\n");
	Exit();
}

int turn_switches(char* switch_nums, char* directions, uint8_t sw_count) {
	ASSERT(sw_count < 23);
	struct Switch_Request req = { .sw_count = 0 };
	for (int i = 0; i < sw_count; i++) {
		char switch_num = switch_nums[i];
		char direction = directions[i];

		if (!((switch_num >= MIN_SWITCH_NUM && switch_num <= MAX_SWITCH_NUM)
			  || (switch_num >= MIN_SWITCH_NUM_MIDDLE && switch_num <= MAX_SWITCH_NUM_MIDDLE))) {
			printi("invalid switch number: ", switch_num, "\r\n");
			return 0;
		}
		if (direction != 'C' && direction != 'S') {
			printi("invalid switch direction on switch: ", switch_num, ", must be one of {'S', 'C'}\r\n");
			return 0;
		}
		if (get_switch_state(switch_num) != direction) {
			req.switch_nums[i] = switch_num;
			req.directions[i] = direction;
			req.sw_count++;
		}
	}

	if (req.sw_count == 0) {
		return 0;
	}
	int worker_tid = Create(2, switch_worker);
	// struct Switch_Request req = { .switch_nums = switch_nums, .directions = directions, .sw_count = sw_count };
	char reply[1];
	Send(worker_tid, (char*)&req, sizeof(struct Switch_Request), reply, 1);
	// prints("flipping switches\r\n");
	return req.sw_count;
}

struct reserve_nodes_request {
	uint8_t nodes_to_reserve[50];
	uint8_t num_nodes_to_reserve;
	int track_admin_tid;
	uint32_t start_time;
	uint32_t end_time;
};

void _reserve_nodes() {
	int sender;
	char rcvbuf[sizeof(struct reserve_nodes_request)];
	char reply = 0;
	struct reserve_nodes_request* rn_req = (struct reserve_nodes_request*)rcvbuf;
	Receive(&sender, rcvbuf, sizeof(struct reserve_nodes_request));
	Reply(sender, &reply, 1);

	ASSERT(rn_req->num_nodes_to_reserve <= 22);
	if (rn_req->start_time >= rn_req->end_time) {
		debug_printi("start_time: ", rn_req->start_time, "\r\n");
		debug_printi("end_time: ", rn_req->end_time, "\r\n");
		debug_printi("nodes_to_reserve:", rn_req->nodes_to_reserve[0], "\r\n");
		ASSERT(0);
		Exit();
	}
	struct Track_Request req
		= { .request_type = TRACK_RESERVE_SEGMENTS, .track_nodes_encoded = { 0 }, .reservation_time = { 0 } };

	req.node_count = rn_req->num_nodes_to_reserve;

	for (uint8_t i = 0; i < rn_req->num_nodes_to_reserve; i++) {
		req.track_nodes_encoded[i] = rn_req->nodes_to_reserve[i];
	}

	req.reservation_time[0] = rn_req->start_time;
	req.reservation_time[1] = rn_req->end_time;

	Send(rn_req->track_admin_tid, (char*)&req, sizeof(struct Track_Request), &reply, 1);
	ASSERT(reply == 0 || reply == 1);
	if (reply == 0) {
		debug_printi("could not reserve segment: ", track_nodes[rn_req->nodes_to_reserve[0]].idx, "\r\n");
	}
	// return reply;
	Exit();
}

int reserve_nodes(uint8_t nodes_to_reserve[50],
				  uint8_t num_nodes_to_reserve,
				  int track_admin_tid,
				  uint32_t start_time,
				  uint32_t end_time) {
	char reply[1];
	struct reserve_nodes_request req = { .num_nodes_to_reserve = num_nodes_to_reserve,
										 .track_admin_tid = track_admin_tid,
										 .start_time = start_time,
										 .end_time = end_time };
	for (int i = 0; i < num_nodes_to_reserve; i++) {
		req.nodes_to_reserve[i] = nodes_to_reserve[i];
	}
	int child_tid = Create(1, _reserve_nodes);
	Send(child_tid, (char*)&req, sizeof(struct reserve_nodes_request), reply, 1);
	return 1;
}

void get_reserved_segments(uint32_t blocked[144][6][2]) {
	struct Track_Request req = { .request_type = TRACK_GET_RESERVATIONS };
	int track_admin_tid = get_tid_by_name(TRACK_ADMIN);
	Send(track_admin_tid, (char*)&req, sizeof(struct Track_Request), (char*)blocked, sizeof(uint32_t) * 144 * 6 * 2);
}

void park_train(uint8_t node, int track_admin_tid) {
	ASSERT(node <= 144);
	struct Track_Request req = { .request_type = TRACK_PARK, .track_nodes_encoded = { 0 }, .reservation_time = { 0 } };

	req.track_nodes_encoded[0] = node;
	req.node_count = 1;

	char reply;
	Send(track_admin_tid, (char*)&req, sizeof(struct Track_Request), &reply, 1);
}

void unpark_train(uint8_t node, int track_admin_tid) {
	ASSERT(node <= 144);
	struct Track_Request req
		= { .request_type = TRACK_UNPARK, .track_nodes_encoded = { 0 }, .reservation_time = { 0 } };
	req.track_nodes_encoded[0] = node;
	req.node_count = 1;

	char reply;
	Send(track_admin_tid, (char*)&req, sizeof(struct Track_Request), &reply, 1);
}

int is_branch_ahead(uint8_t node_idx) {
	ASSERT(node_idx <= 79);
	return (track_nodes[node_idx].edge[DIR_AHEAD].dest->type == NODE_BRANCH);
}
