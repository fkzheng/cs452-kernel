#include "bootstrap.h"

void test_cts() {
	int train_server_w = WhoIs(UART_SERVER_TRAIN_SEND);
	while (train_server_w == -1) {
		train_server_w = WhoIs(UART_SERVER_TRAIN_SEND);
	}
	char msg[] = "        ";
	char speed = 14;
	char train = 74;
	msg[0] = speed;
	msg[1] = train;
	msg[2] = 5 + 16;
	msg[3] = train;
	msg[4] = 5;
	msg[5] = 2;
	msg[6] = 5 + 16;
	msg[7] = 2;
	struct UART_Server_Request req = create_uart_request(UART_SEND_TRAIN, msg);
	char reply[] = " ";
	Send(train_server_w, (char*)&req, sizeof(struct UART_Server_Request), reply, 1);
	Exit();
}

void test_dijkstra_tc2_blocks_properly() {
	// set_path2(74, 56, 0); // D9
	// set_path2(78, 70, 0); // E7
}

// IMPORTANT!!!! THIS DOESN'T TURN SOLENOID OFF!!!!
void set_switch(int uart_tid, int switch_num, char switch_dir) {
	char iswitch_dir = (switch_dir == 'S') ? 33 : 34;
	char cmd[] = { iswitch_dir, switch_num, 0 };

	Puts(uart_tid, 1, cmd);
}

void init_switches() {
	char switch_nums[22];
	char directions[22];

	int i;
	for (i = 0; i < 18; i++) {
		switch_nums[i] = i + 1;
		directions[i] = 'C';
	}

	switch_nums[18] = 153;
	switch_nums[19] = 154;
	switch_nums[20] = 155;
	switch_nums[21] = 156;
	directions[18] = 'S';
	directions[19] = 'C';
	directions[20] = 'S';
	directions[21] = 'C';

	directions[5] = 'S';
	directions[9] = 'S';
	directions[15] = 'S';

	directions[12] = 'S';
	directions[16] = 'S';

	directions[17] = 'S';

	turn_switches(switch_nums, directions, 22);
}

// void reset_switch_states(char switch_state) {
// 	int i;
// 	for (i = 1; i < 19; i++) {
// 		turn_switch(i, switch_state);
// 	}
// 	for (i = 153; i < 157; i++) {
// 		turn_switch(i, switch_state);
// 	}
// }

void create_train_worker(char train) {
	int train_worker_id = Create(1, train_worker);
	char reply[1];
	Send(train_worker_id, &train, 1, reply, 1);
}

static char possible_trains[] = { 1, 2, 24, 58, 74, 78 };

// void test_reservation_task() {

// 	int track_admin_tid = get_tid_by_name(TRACK_ADMIN);
// 	uint8_t nodes_to_reserve[] = { 1, 2, 3 };
// 	reserve_nodes(nodes_to_reserve, (uint8_t)3, track_admin_tid, 5, 6);
// 	reserve_nodes(nodes_to_reserve, (uint8_t)3, track_admin_tid, 7, 8);
// 	reserve_nodes(nodes_to_reserve, (uint8_t)3, track_admin_tid, 9, 10);
// 	reserve_nodes(nodes_to_reserve, (uint8_t)3, track_admin_tid, 11, 12);
// 	reserve_nodes(nodes_to_reserve, (uint8_t)3, track_admin_tid, 13, 14);
// 	reserve_nodes(nodes_to_reserve, (uint8_t)3, track_admin_tid, 15, 16);
// 	reserve_nodes(nodes_to_reserve, (uint8_t)3, track_admin_tid, 17, 18);
// 	Delay(get_tid_by_name(CLOCK_SERVER), 10);
// 	reserve_nodes(nodes_to_reserve, (uint8_t)3, track_admin_tid, 19, 20);
// 	reserve_nodes(nodes_to_reserve, (uint8_t)3, track_admin_tid, 21, 22);

// 	Exit();
// }

void test_rng() {
	uint8_t sensor_idx;
	for (int i = 0; i < 30; i++) {
		char out[50];
		sensor_idx = get_random_sensor();
		str_copy("RANDOMLY GENERATED SENSOR: ", out);
		str_cat(out, track_nodes[sensor_idx].name);
		str_cat(out, "\r\n");
		debug_prints(out);
	}
}

void bootstrap_tc1() {
	init_track('b');
	char NO_CTS = 1;
	Create(0, name_server);
	Create(0, clock_server);
	Create(0, clock_notifier);
	Create(0, uart_term_server_tx);
	if (NO_CTS) {
		Create(1, uart_train_server_tx_no_cts);
	} else {
		Create(1, uart_train_server_tx);
	}
	Create(1, uart_term_server_rx);
	Create(1, uart_train_server_rx);
	Create(0, uart_notifier);
	Create(0, uart_notifier);
	Create(0, uart_notifier);
	Create(0, uart_notifier);
	Create(0, uart_notifier);
	Create(0, uart_notifier);
	Create(4, random_server);
	Create(4, idle_server);

	// A0 servers
	Create(3, timer_server);
	Create(1, track_admin);
	Create(1, train_admin);
	Create(2, track_ui_admin);
	Create(4, print_idle_time_server);

	init_switches();

	Delay(get_tid_by_name(CLOCK_SERVER), 500); // give it 5s to set up all the switches

	Create(1, sensor_query_server);
	if (NO_CTS) {
		Create(1, sensor_read_courier_no_cts);
	} else {
		Create(1, sensor_read_courier);
	}

	Create(1, sensor_query_server);

	for (int i = 0; i < 50; i++) {
		Create(1, terminal_admin);
	}
	for (int i = 0; i < 6; i++) {
		create_train_worker(possible_trains[i]);
	}

	test_dijkstra_tc2_blocks_properly();

	Exit();
}
