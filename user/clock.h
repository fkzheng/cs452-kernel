#ifndef CLOCK_H
#define CLOCK_H
#include "request.h"
#include "syscall.h"

void clock_server();
void clock_notifier();
void timer_server();
#endif