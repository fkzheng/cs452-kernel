#include "syscall.h"

int Create(int priority, void (*function)()) {
	struct Syscall_Request req = create_request(CREATE, (uint64_t)priority, (uint64_t)function, 0, 0, 0);
	return kerent(&req);
}

int MyTid() {
	struct Syscall_Request req = create_request(MY_TID, 0, 0, 0, 0, 0);
	return kerent(&req);
}

int MyParentTid() {
	struct Syscall_Request req = create_request(PARENT_TID, 0, 0, 0, 0, 0);
	return kerent(&req);
}

void Yield() {
	struct Syscall_Request req = create_request(YIELD, 0, 0, 0, 0, 0);
	kerent(&req);
}

uint32_t Idle() {
	struct Syscall_Request req = create_request(IDLE, 0, 0, 0, 0, 0);
	return kerent(&req);
}

void Exit() {
	struct Syscall_Request req = create_request(EXIT, 0, 0, 0, 0, 0);
	kerent(&req);
}

int Send(int tid, const char* msg, int msglen, char* reply, int replylen) {
	struct Syscall_Request req
		= create_request(SEND, (uint64_t)tid, (uint64_t)msg, (uint64_t)msglen, (uint64_t)reply, (uint64_t)replylen);
	return kerent(&req);
}

int Receive(int* tid, char* msg, int msglen) {
	struct Syscall_Request req = create_request(RECEIVE, (uint64_t)tid, (uint64_t)msg, (uint64_t)msglen, 0, 0);
	return kerent(&req);
}

int Reply(int tid, void* reply, int replylen) {
	struct Syscall_Request req = create_request(REPLY, (uint64_t)tid, (uint64_t)reply, (uint64_t)replylen, 0, 0);
	return kerent(&req);
}

int RegisterAs(const char* name) {
	int my_tid = MyTid();
	struct Name_Server_Request ns_request = create_name_server_request(NAME_SERVER_REGISTER, name, my_tid);
	char reply_buf[1];
	Send(g_name_server_tid, (char*)&ns_request, sizeof(struct Name_Server_Request), reply_buf, 1);
	return (int)reply_buf[0];
}

int Unregister() {
	int my_tid = MyTid();
	struct Name_Server_Request ns_request = create_name_server_request(NAME_SERVER_REGISTER, NULL, my_tid);
	char reply_buf[1];
	Send(g_name_server_tid, (char*)&ns_request, sizeof(struct Name_Server_Request), reply_buf, 1);
	return (int)reply_buf[0];
}

int WhoIs(const char* name) {
	struct Name_Server_Request ns_request = create_name_server_request(NAME_SERVER_WHOIS, name, 69);
	char reply_buf[1];
	Send(g_name_server_tid, (char*)&ns_request, sizeof(struct Name_Server_Request), reply_buf, 1);
	// this is kind of trash but what can you do
	if (reply_buf[0] == 255) {
		return -1;
	}
	return (int)reply_buf[0];
}

int AwaitEvent(int event_id) {
	struct Syscall_Request req = create_request(AWAIT_EVENT, (uint64_t)event_id, 0, 0, 0, 0);
	return kerent(&req);
}

int do_clock_server_request(int tid, enum clock_server_request request_type, int arg) {
	struct Clock_Server_Request request = create_clock_request(request_type, arg);
	char reply_buf[sizeof(int)];
	char* send_buf = (char*)&request;
	Send(tid, send_buf, sizeof(struct Clock_Server_Request), reply_buf, sizeof(int));
	int* res_ptr = (int*)(reply_buf);
	return *res_ptr;
}

int UpdateTime(int tid, uint32_t cur_time) {
	return do_clock_server_request(tid, CLOCK_SERVER_UPDATE_TIME, cur_time);
}

int Time(int tid) {
	return do_clock_server_request(tid, CLOCK_SERVER_TIME, 0);
}

int Delay(int tid, int ticks) {
	return do_clock_server_request(tid, CLOCK_SERVER_DELAY, ticks);
}

int DelayUntil(int tid, int ticks) {
	return do_clock_server_request(tid, CLOCK_SERVER_DELAY_UNTIL, ticks);
}

int UartWriteRegister(int channel, char reg, char data) {
	ASSERT(channel == 0 || channel == 1);
	struct Syscall_Request request = create_request(WRITE_REGISTER, channel, reg, data, 0, 0);
	return kerent(&request);
}

int UartReadRegister(int channel, char reg) {
	ASSERT(channel == 0 || channel == 1);
	struct Syscall_Request request = create_request(READ_REGISTER, channel, reg, 0, 0, 0);
	return kerent(&request);
}

int do_uart_server_request(int tid, enum uart_server_request request_type, const char* msg) {
	struct UART_Server_Request request = create_uart_request(request_type, msg);
	char reply_buf[sizeof(int)];
	char* send_buf = (char*)&request;
	Send(tid, send_buf, sizeof(struct UART_Server_Request), reply_buf, sizeof(int));
	int* res_ptr = (int*)(reply_buf);
	return *res_ptr;
}

int Getc(int tid, int uart_channel) {
	switch (uart_channel) {
	case 0:
		return do_uart_server_request(tid, UART_GET_TERM, "G");
		break;
	case 1:
		return do_uart_server_request(tid, UART_GET_TRAIN, "G");
		break;
	default:
		ASSERT(0);
		break;
	}
	return -1;
}

int Puts(int tid, int uart_channel, const char* msg) {
	switch (uart_channel) {
	case 0:
		return do_uart_server_request(tid, UART_SEND_TERM, msg);
		break;
	case 1:
		return do_uart_server_request(tid, UART_SEND_TRAIN, msg);
		break;
	default:
		ASSERT(0);
		break;
	}
	return -1;
}

int Putc(int tid, int uart_channel, char c) {
	char msg[] = "_";
	msg[0] = c;
	switch (uart_channel) {
	case 0:
		return do_uart_server_request(tid, UART_SEND_TERM, msg);
		break;
	case 1:
		return do_uart_server_request(tid, UART_SEND_TRAIN, msg);
		break;
	default:
		ASSERT(0);
		break;
	}
	return -1;
}

void GetNextTerminalCommand(int terminal_rx_server_tid, char* buf) {
	struct UART_Server_Request req = create_uart_request(UART_GET_TERM, "1");
	Send(terminal_rx_server_tid, (char*)&req, sizeof(struct UART_Server_Request), buf, 100);
}

int GetIdleTime() {
	struct Syscall_Request req = create_request(GET_IDLE_TIME, 0, 0, 0, 0, 0);
	return kerent(&req);
}

void Quit() {
	struct Syscall_Request req = create_request(QUIT, 0, 0, 0, 0, 0);
	kerent(&req);
}