#ifndef TRAIN_TESTS
#define TRAIN_TESTS
#include "pathfinding.h"
#include "track_info/track.h"
#include "util/debug.h"
#include "util/ring_buffer.h"
#include "util/util.h"

struct Ring_Buffer;
void test_dijkstra(int src, int dest);
void print_path(char* prefix, struct Ring_Buffer* buf);

#endif