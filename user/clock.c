#include "clock.h"

void clock_server() {
	RegisterAs(CLOCK_SERVER);
	int sender_tid;
	char rcvbuf[sizeof(struct Clock_Server_Request)];
	char* replybuf;

	// store request as tid / reply
	struct Ring_Buffer reply_tids = create_buffer(INT);
	struct Ring_Buffer replies = create_buffer(INT);
	uint32_t reply = 0;
	uint32_t cur_time = 0;

	for (;;) {
		Receive(&sender_tid, rcvbuf, sizeof(struct Clock_Server_Request));
		// handle request
		struct Clock_Server_Request* req = (struct Clock_Server_Request*)rcvbuf;
		switch (req->request_type) {
		case CLOCK_SERVER_TIME:
			reply = cur_time;
			break;
		case CLOCK_SERVER_DELAY:
			reply = cur_time + req->arg1;
			break;
		case CLOCK_SERVER_DELAY_UNTIL:
			reply = req->arg1;
			break;
		case CLOCK_SERVER_UPDATE_TIME:
			cur_time = req->arg1;
			reply = cur_time;
			break;
		default:
			ASSERT(0);
		}

		ASSERT(!ring_buffer_is_full(&reply_tids));
		ASSERT(!ring_buffer_is_full(&replies));
		ring_buffer_append(&reply_tids, sender_tid);
		ring_buffer_append(&replies, reply);

		// cycle through all replies and send the ones that can be sent
		int size = replies.size;
		for (int i = 0; i < size; i++) {
			ASSERT(!ring_buffer_is_empty(&replies));
			ASSERT(!ring_buffer_is_empty(&reply_tids));
			uint32_t reply = ring_buffer_pop(&replies);
			uint32_t reply_tid = ring_buffer_pop(&reply_tids);

			if (reply <= cur_time) {
				replybuf = (char*)&reply;
				Reply(reply_tid, replybuf, sizeof(int));
			} else {
				ASSERT(!ring_buffer_is_full(&reply_tids));
				ASSERT(!ring_buffer_is_full(&replies));
				ring_buffer_append(&replies, reply);
				ring_buffer_append(&reply_tids, reply_tid);
			}
		}
	}
}

void clock_notifier() {
	RegisterAs(CLOCK_NOTIFIER);
	int clock_server_tid = get_tid_by_name(CLOCK_SERVER);

	// this loop must execute in less than 10ms (1 tick)
	for (;;) {
		AwaitEvent(97);
		update_timer_1();
		ack_interrupt(97);
		uint32_t cur_time = get_timestamp();
		UpdateTime(clock_server_tid, cur_time);
	}
}

void print_timer(uint32_t timestamp) {
	char time[] = "MMM:SS:m";
	char prefix[] = "\0337\033[2;1H\033[32mSystem Uptime: ";
	char suffix[] = "\033[0m\0338";
	char timer_str[sizeof(prefix) + sizeof(time) + sizeof(suffix) + 1];
	// printi(" ", timestamp, "\r\n");
	timestamp /= 10;
	time[7] = timestamp % 10 + '0';
	timestamp /= 10;
	time[5] = timestamp % 10 + '0';
	timestamp /= 10;
	time[4] = timestamp % 6 + '0';
	timestamp /= 6;
	time[2] = timestamp % 10 + '0';
	timestamp /= 10;
	time[1] = timestamp % 10 + '0';
	timestamp /= 10;
	time[0] = timestamp % 10 + '0';

	str_copy(prefix, timer_str);
	str_cat(timer_str, time);
	str_cat(timer_str, suffix);

	prints(timer_str);
}

void timer_server() {
	int clock_server_tid = WhoIs(CLOCK_SERVER);
	while (clock_server_tid == -1) {
		clock_server_tid = WhoIs(CLOCK_SERVER);
	}

	// number of 10ms ticks since start
	uint32_t target_time = Time(clock_server_tid);

	for (;;) {
		target_time += 10;
		// 10*10ms ticks = 100ms
		DelayUntil(clock_server_tid, target_time);

		// print the time
		print_timer(target_time);
	}

	ASSERT(0);
	Exit();
}
