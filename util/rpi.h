#ifndef RPI_H
#define RPI_H

#include "debug.h"
#include <stddef.h>
#include <stdint.h>

#define CLOCKHZ 1000000

void init_gpio();
void init_spi(uint32_t channel);
void init_uart(uint32_t spiChannel);
void init_gic();
// TODO: standardize snake case for kernel stuff and upper camal case for syscall
char UartGetc(size_t uartChannel);
void UartPutc(size_t spiChannel, size_t uartChannel, char c);
void uart_puts(size_t spiChannel, size_t uartChannel, const char* buf, size_t blen);

int can_send_char(size_t uartChannel);
int can_read_char(size_t uartChannel);

void init_timer();
void update_timer_1();
uint32_t get_timestamp();
uint32_t get_true_timestamp();

uint32_t get_interrupt_number();
void ack_interrupt(uint32_t ack);
void ack_uart_interrupt();

void enable_tx_interrupt(size_t uart_channel);
void disable_tx_interrupt(size_t uart_channel);

void kernel_enable_rx_interrupt(char uart_channel);
void kernel_disable_rx_interrupt(char uart_channel);
void enable_rx_interrupt(char uart_channel);

void enable_cts_interrupt(size_t uart_channel);
void kernel_disable_cts_interrupt(size_t uart_channel);
void enable_modem_interrupt(size_t uart_channel);
void kernel_disable_modem_interrupt(size_t uart_channel);

char read_uart_irq(int uart_channel);

void kernel_disable_tx_interrupt(char uart_channel);

void uart_write_register(size_t spiChannel, size_t uartChannel, char reg, char data);
char uart_read_register(size_t spiChannel, size_t uartChannel, char reg);

int is_cts_delta();
#endif