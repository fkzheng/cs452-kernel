#ifndef DEBUG_H
#define DEBUG_H

#include "rpi.h"
#include "user/syscall.h"
#include <stdint.h>

#define ASSERT(expr)                                                                                                   \
	if (!(expr))                                                                                                       \
	aFailed(__FILE__, __LINE__)

void aFailed(char* file, int line);
void printi(char* prefix, uint64_t n, char* suffix);
void printi_maybe_negative(char* prefix, int n, char* suffix);
void prints(char* msg);

// prints in debug region
void debug_printi_maybe_negative(char* prefix, int i, char* suffix);
void debug_printi(char* prefix, uint64_t n, char* suffix);
void debug_prints(char* msg);

void itos(int n, char* s, int want_null_terminator);
// void print_idle_time(uint32_t idle_time);
void kernel_prints(char* msg);
void kernel_printi(uint64_t n);
int charsize(const char* ptr);

#endif
