#include "rpi.h"

struct GPIO {
	uint32_t GPFSEL[6];
	uint32_t : 32;
	uint32_t GPSET0;
	uint32_t GPSET1;
	uint32_t : 32;
	uint32_t GPCLR0;
	uint32_t GPCLR1;
	uint32_t : 32;
	uint32_t GPLEV0;
	uint32_t GPLEV1;
	uint32_t : 32;
	uint32_t GPEDS0;
	uint32_t GPEDS1;
	uint32_t : 32;
	uint32_t GPREN0;
	uint32_t GPREN1;
	uint32_t : 32;
	uint32_t GPFEN0;
	uint32_t GPFEN1;
	uint32_t : 32;
	uint32_t GPHEN0;
	uint32_t GPHEN1;
	uint32_t : 32;
	uint32_t GPLEN0;
	uint32_t GPLEN1;
	uint32_t : 32;
	uint32_t GPAREN0;
	uint32_t GPAREN1;
	uint32_t : 32;
	uint32_t GPAFEN0;
	uint32_t GPAFEN1;
	uint32_t _unused[21];
	uint32_t PUP_PDN_CNTRL_REG[4];
};

struct AUX {
	uint32_t IRQ;
	uint32_t ENABLES;
};

struct SPI {
	uint32_t CNTL0; // Control register 0
	uint32_t CNTL1; // Control register 1
	uint32_t STAT;	// Status
	uint32_t PEEK;	// Peek
	uint32_t _unused[4];
	uint32_t IO_REGa;	  // Data
	uint32_t IO_REGb;	  // Data
	uint32_t IO_REGc;	  // Data
	uint32_t IO_REGd;	  // Data
	uint32_t TXHOLD_REGa; // Extended Data
	uint32_t TXHOLD_REGb; // Extended Data
	uint32_t TXHOLD_REGc; // Extended Data
	uint32_t TXHOLD_REGd; // Extended Data
};

struct TIMER {
	uint32_t CS;  // System Timer Control/Status
	uint32_t CLO; // System Timer Counter Lower 32 bits
	uint32_t CHI; // System Timer Counter Higher 32 bits
	uint32_t C0;  // System Timer Compare 0 // reserved for GPU
	uint32_t C1;  // System Timer Compare 1
	uint32_t C2;  // System Timer Compare 2 // reserved for GPU
	uint32_t C3;  // System Timer Compare 3
};

struct GICD {
	uint32_t GICD_CTLR;				 // 0x0
	uint32_t GICD_TYPER;			 // 0x4
	uint32_t GICD_IIDR;				 // 0x8
	uint32_t _unused1[5];			 // 0xC, 0x1C
	uint32_t _unused2[8];			 // 0x20, 0x3C
	uint32_t _unused3[16];			 // 0x40, 0x7C
	uint32_t GICD_IGROUPRn[32];		 // 0x80, 0xFC
	uint32_t GICD_ISENABLERn[32];	 // 0x100, 0x17C
	uint32_t GICD_ICENABLERn[32];	 // 0x180, 0x1FC
	uint32_t GICD_ISPENDRn[32];		 // 0x200, 0x27C
	uint32_t GICD_ICPENDRn[32];		 // 0x280, 0x2FC
	uint32_t GICD_ISACTIVERn[32];	 // 0x300, 0x37C
	uint32_t GICD_ICACTIVERn[32];	 // 0x380, 0x3FC
	uint32_t GICD_IPRIORITYRn[255];	 // 0x400, 0x7F8
	uint32_t _unused4;				 // 0x7FC
	uint8_t GICD_ITARGETSRn_RO[32];	 // 0x800, 0x81C
	uint8_t GICD_ITARGETSRn_RW[988]; // 0x820, 0xBF8
	uint32_t _unused5;				 // 0xBFC
	uint32_t GICD_ICFGRn[64];		 // 0xC00, 0xCFC
	uint32_t _unused6[64];			 // 0xC00, 0xCFC
	uint32_t GICD_NSACRn[64];		 // 0xE00, 0xEFC
	uint32_t GICD_SGIR;				 // 0xF00
	uint32_t _unused7[3];			 // 0xF04-0xF0C
	uint32_t GICD_CPENDSGIRn;		 // 0xF10-0xF1C
	uint32_t GICD_SPENDSGIRn;		 // 0xF20-0xF2C
};

struct GICC {
	uint32_t GICC_CTLR;	   // 0x0
	uint32_t GICC_PMR;	   // 0x4
	uint32_t GICC_BPR;	   // 0x8
	uint32_t GICC_IAR;	   // 0xC
	uint32_t GICC_EOIR;	   // 0x10
	uint32_t GICC_RPR;	   // 0x14
	uint32_t GICC_HPPIR;   // 0x18
	uint32_t GICC_ABPR;	   // 0x1C
	uint32_t GICC_AIAR;	   // 0x20
	uint32_t GICC_AEOIR;   // 0x24
	uint32_t GICC_AHPPIR;  // 0x28
	uint32_t _unused1[4];  // 0x2C - 0x3C
	uint32_t _unused2[35]; // 0x40 - 0xCF
	uint32_t GICC_APRn[4]; // 0xD0 - 0xDC
	uint32_t GICC_NSAPRn;  // 0xE0 - 0xEC
	uint32_t _unused3[3];  // 0xF0 - 0xF8
	uint32_t GICC_IIDR;	   // 0xFC
	uint32_t GICC_DIR;	   // 0x100
};

static char* const MMIO_BASE = (char*)0xFE000000;
static char* const GPIO_BASE = (char*)(MMIO_BASE + 0x200000);
static char* const AUX_BASE = (char*)(GPIO_BASE + 0x15000);
static char* const GIC_BASE = (char*)0xFF840000;

static volatile struct GPIO* const gpio = (struct GPIO*)(GPIO_BASE);
static volatile struct AUX* const aux = (struct AUX*)(AUX_BASE);
static volatile struct SPI* const spi[] = { (struct SPI*)(AUX_BASE + 0x80), (struct SPI*)(AUX_BASE + 0xC0) };
static volatile struct TIMER* const timer = (struct TIMER*)(0xFE003000);

static volatile struct GICD* const gicd = (struct GICD*)(GIC_BASE + 0x1000);
static volatile struct GICC* const gicc = (struct GICC*)(GIC_BASE + 0x2000);

/*************** TIMER ***************/
const uint32_t interval_1 = CLOCKHZ / 100;
uint32_t timer_start_val = 0;
uint32_t timer_1_val = 0;

void init_timer() {
	timer_start_val = timer_1_val = timer->CLO;
	timer_1_val += interval_1;
	timer->C1 = timer_1_val;
}

void update_timer_1() {
	timer_1_val += interval_1;
	timer->C1 = timer_1_val;
	timer->CS |= (1 << 1);
}

// get time from start of program in ticks
uint32_t get_timestamp() {
	return (timer->CLO - timer_start_val) / interval_1;
}

// get time from start of program in microseconds
uint32_t get_true_timestamp() {
	return (timer->CLO - timer_start_val);
}

/*************** GPIO ***************/

static const uint32_t GPIO_INPUT = 0x00;
static const uint32_t GPIO_OUTPUT = 0x01;
static const uint32_t GPIO_ALTFN0 = 0x04;
static const uint32_t GPIO_ALTFN1 = 0x05;
static const uint32_t GPIO_ALTFN2 = 0x06;
static const uint32_t GPIO_ALTFN3 = 0x07;
static const uint32_t GPIO_ALTFN4 = 0x03;
static const uint32_t GPIO_ALTFN5 = 0x02;

static const uint32_t GPIO_NONE = 0x00;
static const uint32_t GPIO_PUP = 0x01;
static const uint32_t GPIO_PDP = 0x02;

static void setup_gpio(uint32_t pin, uint32_t setting, uint32_t resistor) {
	uint32_t reg = pin / 10;
	uint32_t shift = (pin % 10) * 3;
	uint32_t status = gpio->GPFSEL[reg]; // read status
	status &= ~(7u << shift);			 // clear bits
	status |= (setting << shift);		 // set bits
	gpio->GPFSEL[reg] = status;			 // write back

	reg = pin / 16;
	shift = (pin % 16) * 2;
	status = gpio->PUP_PDN_CNTRL_REG[reg]; // read status
	status &= ~(3u << shift);			   // clear bits
	status |= (resistor << shift);		   // set bits
	gpio->PUP_PDN_CNTRL_REG[reg] = status; // write back
}

void init_gpio() {
	setup_gpio(18, GPIO_ALTFN4, GPIO_NONE);
	setup_gpio(19, GPIO_ALTFN4, GPIO_NONE);
	setup_gpio(20, GPIO_ALTFN4, GPIO_NONE);
	setup_gpio(21, GPIO_ALTFN4, GPIO_NONE);
	setup_gpio(24, GPIO_INPUT, GPIO_NONE);
	gpio->GPLEN0 |= (1 << 24);
}

static const uint32_t SPI_CNTL0_DOUT_HOLD_SHIFT = 12;
static const uint32_t SPI_CNTL0_CS_SHIFT = 17;
static const uint32_t SPI_CNTL0_SPEED_SHIFT = 20;

static const uint32_t SPI_CNTL0_POSTINPUT = 0x00010000;
static const uint32_t SPI_CNTL0_VAR_CS = 0x00008000;
static const uint32_t SPI_CNTL0_VAR_WIDTH = 0x00004000;
static const uint32_t SPI_CNTL0_Enable = 0x00000800;
static const uint32_t SPI_CNTL0_In_Rising = 0x00000400;
static const uint32_t SPI_CNTL0_Clear_FIFOs = 0x00000200;
static const uint32_t SPI_CNTL0_Out_Rising = 0x00000100;
static const uint32_t SPI_CNTL0_Invert_CLK = 0x00000080;
static const uint32_t SPI_CNTL0_SO_MSB_FST = 0x00000040;
static const uint32_t SPI_CNTL0_MAX_SHIFT = 0x0000003F;

static const uint32_t SPI_CNTL1_CS_HIGH_SHIFT = 8;

static const uint32_t SPI_CNTL1_Keep_Input = 0x00000001;
static const uint32_t SPI_CNTL1_SI_MSB_FST = 0x00000002;
static const uint32_t SPI_CNTL1_Done_IRQ = 0x00000040;
static const uint32_t SPI_CNTL1_TX_EM_IRQ = 0x00000080;

static const uint32_t SPI_STAT_TX_FIFO_MASK = 0xFF000000;
static const uint32_t SPI_STAT_RX_FIFO_MASK = 0x00FF0000;
static const uint32_t SPI_STAT_TX_FULL = 0x00000400;
static const uint32_t SPI_STAT_TX_EMPTY = 0x00000200;
static const uint32_t SPI_STAT_RX_FULL = 0x00000100;
static const uint32_t SPI_STAT_RX_EMPTY = 0x00000080;
static const uint32_t SPI_STAT_BUSY = 0x00000040;
static const uint32_t SPI_STAT_BIT_CNT_MASK = 0x0000003F;

void init_spi(uint32_t channel) {
	uint32_t reg = aux->ENABLES;
	reg |= (2 << channel);
	aux->ENABLES = reg;
	spi[channel]->CNTL0 = SPI_CNTL0_Clear_FIFOs;
	uint32_t speed = (700000000 / (2 * 0x400000)) - 1; // for maximum bitrate 0x400000
	spi[channel]->CNTL0 = (speed << SPI_CNTL0_SPEED_SHIFT) | SPI_CNTL0_VAR_WIDTH | SPI_CNTL0_Enable
						  | SPI_CNTL0_In_Rising | SPI_CNTL0_SO_MSB_FST;
	spi[channel]->CNTL1 = SPI_CNTL1_SI_MSB_FST;
}

static void spi_send_recv(uint32_t channel, const char* sendbuf, size_t sendlen, char* recvbuf, size_t recvlen) {
	size_t sendidx = 0;
	size_t recvidx = 0;
	while (sendidx < sendlen || recvidx < recvlen) {
		uint32_t data = 0;
		size_t count = 0;

		// prepare write data
		for (; sendidx < sendlen && count < 24; sendidx += 1, count += 8) {
			data |= (sendbuf[sendidx] << (16 - count));
		}
		data |= (count << 24);

		// always need to write something, otherwise no receive
		while (spi[channel]->STAT & SPI_STAT_TX_FULL)
			asm volatile("yield");
		if (sendidx < sendlen) {
			spi[channel]->TXHOLD_REGa = data; // keep chip-select active, more to come
		} else {
			spi[channel]->IO_REGa = data;
		}

		// read transaction
		while (spi[channel]->STAT & SPI_STAT_RX_EMPTY)
			asm volatile("yield");
		data = spi[channel]->IO_REGa;

		// process data, if needed, assume same byte count in transaction
		size_t max = (recvlen - recvidx) * 8;
		if (count > max)
			count = max;
		for (; count > 0; recvidx += 1, count -= 8) {
			recvbuf[recvidx] = (data >> (count - 8)) & 0xFF;
		}
	}
}

/*************** SPI ***************/

static const char UART_RHR = 0x00;		// R
static const char UART_THR = 0x00;		// W
static const char UART_IER = 0x01;		// R/W
static const char UART_IIR = 0x02;		// R
static const char UART_FCR = 0x02;		// W
static const char UART_LCR = 0x03;		// R/W
static const char UART_MCR = 0x04;		// R/W
static const char UART_LSR = 0x05;		// R
static const char UART_MSR = 0x06;		// R
static const char UART_SPR = 0x07;		// R/W
static const char UART_TXLVL = 0x08;	// R
static const char UART_RXLVL = 0x09;	// R
static const char UART_IODir = 0x0A;	// R/W
static const char UART_IOState = 0x0B;	// R/W
static const char UART_IOIntEna = 0x0C; // R/W
static const char UART_reserved = 0x0D;
static const char UART_IOControl = 0x0E; // R/W
static const char UART_EFCR = 0x0F;		 // R/W

static const char UART_DLL = 0x00; // R/W - only accessible when EFR[4] = 1 and MCR[2] = 1
static const char UART_DLH = 0x01; // R/W - only accessible when EFR[4] = 1 and MCR[2] = 1
static const char UART_EFR = 0x02; // ?   - only accessible when LCR is 0xBF
static const char UART_TCR = 0x06; // R/W - only accessible when EFR[4] = 1 and MCR[2] = 1
static const char UART_TLR = 0x07; // R/W - only accessible when EFR[4] = 1 and MCR[2] = 1

// UART flags
static const char UART_CHANNEL_SHIFT = 1;
static const char UART_ADDR_SHIFT = 3;
static const char UART_READ_ENABLE = 0x80;
static const char UART_FCR_TX_FIFO_RESET = 0x04;
static const char UART_FCR_RX_FIFO_RESET = 0x02;
static const char UART_FCR_FIFO_EN = 0x01;
static const char UART_LCR_DIV_LATCH_EN = 0x80;
static const char UART_EFR_ENABLE_ENHANCED_FNS = 0x10;
static const char UART_IOControl_RESET = 0x08;
static const char UART_IER_RX_ENABLE = 1;
static const char UART_IER_TX_ENABLE = 2;
static const char UART_IER_CTS_ENABLE = 0x80;
static const char UART_IER_MODEM_ENABLE = (1 << 3);

void uart_write_register(size_t spiChannel, size_t uartChannel, char reg, char data) {
	char req[2] = { 0 };
	req[0] = (uartChannel << UART_CHANNEL_SHIFT) | (reg << UART_ADDR_SHIFT);
	req[1] = data;
	spi_send_recv(spiChannel, req, 2, NULL, 0);
}

char uart_read_register(size_t spiChannel, size_t uartChannel, char reg) {
	char req[2] = { 0 };
	char res[2] = { 0 };
	req[0] = (uartChannel << UART_CHANNEL_SHIFT) | (reg << UART_ADDR_SHIFT) | UART_READ_ENABLE;
	spi_send_recv(spiChannel, req, 2, res, 2);
	return res[1];
}

char read_uart_irq(int uart_channel) {
	return uart_read_register(0, uart_channel, UART_IIR);
}

static void uart_init_channel(size_t spiChannel, size_t uartChannel, size_t baudRate) {
	// set baud rate
	uart_write_register(spiChannel, uartChannel, UART_LCR, UART_LCR_DIV_LATCH_EN);
	uint32_t bauddiv = 14745600 / (baudRate * 16);
	uart_write_register(spiChannel, uartChannel, UART_DLH, ((bauddiv & 0xFF00) >> 8));
	uart_write_register(spiChannel, uartChannel, UART_DLL, (bauddiv & 0x00FF));

	// set serial byte configuration: 8 bit, no parity, 1 stop bit
	char data = 0x3;
	if (uartChannel == 1) {
		data = 0x7; // two stop bits for marklin
	}
	uart_write_register(spiChannel, uartChannel, UART_LCR, data);

	// clear and enable fifos, (wait since clearing fifos takes time)
	uart_write_register(
		spiChannel, uartChannel, UART_FCR, UART_FCR_RX_FIFO_RESET | UART_FCR_TX_FIFO_RESET | UART_FCR_FIFO_EN);
	for (int i = 0; i < 65535; ++i)
		asm volatile("yield");
}

void init_uart(uint32_t spiChannel) {
	uart_write_register(spiChannel, 0, UART_IOControl, UART_IOControl_RESET); // resets both channels
	uart_write_register(spiChannel, 1, UART_IOControl, UART_IOControl_RESET);
	uart_write_register(spiChannel, 1, UART_IOControl, 3);
	// Configure FIFO Interrupts for channel 0 ?
	uart_write_register(spiChannel, 0, UART_LCR, 0xBF);
	uart_write_register(spiChannel, 0, UART_EFR, (1 << 4));
	uart_write_register(spiChannel, 0, UART_MCR, (1 << 2));
	// customize THR/RHR interrupt threshold with this set, defaults to FCR[4:3] value
	uart_write_register(spiChannel, 0, UART_TLR, 1);		// value*4 spaces
	uart_write_register(spiChannel, 0, UART_TLR, (1 << 4)); // value*4 characters RX FIFO

	// Configure FIFO Interrupts for channel 1 ?
	uart_write_register(spiChannel, 1, UART_LCR, 0xBF);
	uart_write_register(spiChannel, 1, UART_EFR, (1 << 4));
	uart_write_register(spiChannel, 1, UART_MCR, (1 << 2));
	// customize THR/RHR interrupt threshold with this set, defaults to FCR[4:3] value
	uart_write_register(spiChannel, 1, UART_TLR, 1);		// value*4 spaces TX FIFO
	uart_write_register(spiChannel, 1, UART_TLR, (1 << 4)); // value*4 charcters RX FIFO

	uart_init_channel(spiChannel, 0, 115200);
	uart_init_channel(spiChannel, 1, 2400);
}

int can_read_char(size_t uartChannel) {
	if (UartReadRegister(uartChannel, UART_RXLVL) == 0)
		return 0;
	return 1;
}

char UartGetc(size_t uartChannel) {
	return UartReadRegister(uartChannel, UART_RHR);
}

int can_send_char(size_t uartChannel) {
	return UartReadRegister(uartChannel, UART_TXLVL) != 0;
}

void UartPutc(size_t spiChannel, size_t uartChannel, char c) {
	(void)spiChannel;
	UartWriteRegister(uartChannel, UART_THR, c);
}

void uart_puts(size_t spiChannel, size_t uartChannel, const char* buf, size_t blen) {
	static const size_t max = 32;
	char temp[max];
	temp[0] = (uartChannel << UART_CHANNEL_SHIFT) | (UART_THR << UART_ADDR_SHIFT);
	size_t tlen = uart_read_register(spiChannel, uartChannel, UART_TXLVL);
	if (tlen > max)
		tlen = max;
	for (size_t bidx = 0, tidx = 1;;) {
		if (tidx < tlen && bidx < blen) {
			temp[tidx] = buf[bidx];
			bidx += 1;
			tidx += 1;
		} else {
			spi_send_recv(spiChannel, temp, tidx, NULL, 0);
			if (bidx == blen)
				break;
			tlen = uart_read_register(spiChannel, uartChannel, UART_TXLVL);
			if (tlen > max)
				tlen = max;
			tidx = 1;
		}
	}
}

void init_gic() {
	for (int i = 0; i < 32; i++) {
		gicd->GICD_ISENABLERn[i] = 0;
	}
	// init timer interrupt
	gicd->GICD_ISENABLERn[3] = (1 << 1);
	// init0 uart interrupts
	gicd->GICD_ISENABLERn[4] |= (1 << 17);

	for (int i = 0; i < 988; i++) {
		gicd->GICD_ITARGETSRn_RW[i] = 1;
	}
}

void enable_tx_interrupt(size_t uart_channel) {
	// start with only write  ready
	UartWriteRegister(uart_channel, UART_IER, UartReadRegister(uart_channel, UART_IER) | UART_IER_TX_ENABLE);
}

void kernel_disable_tx_interrupt(char uart_channel) {
	uart_write_register(0, uart_channel, UART_IER, uart_read_register(0, uart_channel, UART_IER) & ~UART_IER_TX_ENABLE);
}

void disable_tx_interrupt(size_t uart_channel) {
	// UartWriteRegister(uart_channel, UART_IER, 0);
	UartWriteRegister(uart_channel, UART_IER, UartReadRegister(uart_channel, UART_IER) & ~UART_IER_TX_ENABLE);
}

void enable_rx_interrupt(char uart_channel) {
	UartWriteRegister(uart_channel, UART_IER, UartReadRegister(uart_channel, UART_IER) | UART_IER_RX_ENABLE);
}

void kernel_enable_rx_interrupt(char uart_channel) {
	// start with only write ready
	uart_write_register(0, uart_channel, UART_IER, uart_read_register(0, uart_channel, UART_IER) | UART_IER_RX_ENABLE);
}

void kernel_disable_rx_interrupt(char uart_channel) {
	uart_write_register(0, uart_channel, UART_IER, uart_read_register(0, uart_channel, UART_IER) & ~UART_IER_RX_ENABLE);
}

void enable_cts_interrupt(size_t uart_channel) {
	UartWriteRegister(uart_channel, UART_IER, UartReadRegister(uart_channel, UART_IER) | UART_IER_CTS_ENABLE);
}

void kernel_disable_cts_interrupt(size_t uart_channel) {
	uart_write_register(
		0, uart_channel, UART_IER, uart_read_register(0, uart_channel, UART_IER) & ~UART_IER_CTS_ENABLE);
}

void enable_modem_interrupt(size_t uart_channel) {
	UartWriteRegister(uart_channel, UART_IER, UartReadRegister(uart_channel, UART_IER) | UART_IER_MODEM_ENABLE);
}

void kernel_disable_modem_interrupt(size_t uart_channel) {
	uart_write_register(
		0, uart_channel, UART_IER, uart_read_register(0, uart_channel, UART_IER) & ~UART_IER_MODEM_ENABLE);
}

uint32_t get_interrupt_number() {
	return gicc->GICC_IAR;
}

void ack_interrupt(uint32_t ack) {
	gicc->GICC_EOIR |= ack;
}

void ack_uart_interrupt() {
	gicc->GICC_EOIR |= 145;
	gpio->GPEDS0 |= (1 << 24);
}

int last_msr = 20;

int is_cts_delta() {
	char msr_val = uart_read_register(0, 1, UART_MSR);
	// if (last_msr == 20){
	// 	last_msr = msr_val;
	// } else {
	// 	if (last_msr == msr_val){
	// 		kernel_prints("got duplicated msr\r\n");
	// 	}
	// 	last_msr = msr_val;
	// }
	return msr_val & 1;
}