#ifndef IRQ_H
#define IRQ_H

#include "task.h"
#include "user/request.h"
#include "util/debug.h"
#include "util/rpi.h"
#include <stdint.h>

// Interrupts
#define UART_INTERRUPT 145
#define TIMER_INTERRUPT 97
#define GIC_SPURIOUS_ACK 1023

struct Syscall_Request* handle_interrupt();

#endif