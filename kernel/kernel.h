#ifndef KERNEL_H
#define KERNEL_H
#include "task.h"
#include "user/bootstrap.h" // used for initialize_kernel
#include "user/request.h"
#include "util/util.h"

extern struct Syscall_Request* enter_user_mode(struct Task* currtask);

struct Syscall_Request* activate(struct Task* currtask);
int handle(struct Syscall_Request* request, struct Task* currtask);
void setup_kernel_tasks();
void kmain();
void init_idle_time_tracking();

#endif
