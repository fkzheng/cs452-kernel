#include "mem_allocator.h"

struct Empty_Section {
	void* next;
};

struct Empty_Section* g_head = (struct Empty_Section*)0x10000000;
char* g_max_address = (char*)0x70000000;

int g_section_size = 128000; // 128 kb

void init_memory() {
	char* prev = (char*)g_head;
	char* next = (char*)g_head;
	while (prev < g_max_address) {
		next += g_section_size;
		struct Empty_Section* e = (struct Empty_Section*)prev;
		e->next = next;
		prev = next;
	}
}

void free_section(void* section) {
	struct Empty_Section* new_head = (struct Empty_Section*)section;
	new_head->next = g_head;
	g_head = new_head;
}

void* get_next_free_section() {
	void* res = (void*)g_head;
	g_head = g_head->next;
	return res;
}

char* bottom_of_section(void* section) {
	char* ret = (char*)section;
	return (char*)ret + g_section_size;
}