#ifndef TRAIN_VELOCITY_H
#define TRAIN_VELOCITY_H
#include "stdint.h"
#include "util/debug.h"

// units:
// distance: mm
// time: ticks
// v_0: mm/s
// v_1 mm/s
// returns: mm/s^2
int acceleration_calculation(int distance, int time, int v_0, int v_1);

// units:
// velocity: mm/s
// acceleration: mm/s^2 (must be parity sign as velocity)
// returns: ticks
int stopping_time_from_acceleration(int velocity, int acceleration);

// units:
// velocity: mm/2
// acceleration: mm/s^2 (must be opposite parity as velocity)
// returns: mm
int stopping_distance_from_acceleration(int velocity, int acceleration);

struct fraction {
	int numerator;
	int denominator;
};

struct fraction fraction_multiply(struct fraction f1, struct fraction f2);
struct fraction fraction_divide(struct fraction f1, struct fraction f2);
struct fraction fraction_add(struct fraction f1, struct fraction f2);
struct fraction fraction_subtract(struct fraction f1, struct fraction f2);

#endif